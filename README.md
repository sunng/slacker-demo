# Slacker starter example

This is a practical example of [slacker
framework](https://github.com/sunng87/slacker), both client and server. 

## Execution

### server

    lein run

### client

    lein run programming

