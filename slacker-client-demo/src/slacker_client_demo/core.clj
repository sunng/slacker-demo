(ns slacker-client-demo.core
  (:use [clojure.pprint :only [pprint]])
  (:use [slacker-client-demo.client])
  (:use [slacker.client]))

(use-remote 'scp 'redday.api)
(use-remote 'scp 'redday.stats)

(defn -main [subreddit & args]
  (let [subreddit (or subreddit "programming")
        reddits (top-titles subreddit)
        words-stats (apply nouns-count reddits)]
    (pprint words-stats)))

