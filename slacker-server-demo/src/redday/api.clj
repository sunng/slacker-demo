(ns redday.api
  (:require [reddit.clj.core :as reddit]))

(defn top-titles [subreddit]
  (let [reddit-client (reddit/login)]
    (map :title (reddit/reddits reddit-client subreddit))))

(defn user-titles [name]
  (let [reddit-client (reddit/login)]
    (map :title
         (filter #(.startsWith (:name %) "t3")
                 (reddit/user reddit-client name)))))

