(ns redday.stats
  (:require [clojure.string :as string])
  (:require [opennlp.nlp :as nlp])
  (:require [opennlp.tools.filters :as nlp-filters]))

(def ^{:private true} tokenizer (nlp/make-tokenizer "model/en-token.bin"))
(def ^{:private true} pos-tag (nlp/make-pos-tagger "model/en-pos-maxent.bin"))

(defn- filter-words [sentence]
  (map first (nlp-filters/nouns (pos-tag (tokenizer sentence)))))

(defn nouns-count [& sentences]
  (let [words (mapcat filter-words (map string/lower-case sentences))]
    (frequencies words)))
