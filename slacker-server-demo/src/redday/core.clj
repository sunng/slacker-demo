(ns redday.core
  (:require [redday stats api])
  (:use [slacker.server])
  (:use [slacker.interceptor])
  (:use [slacker.interceptors.exectime :only [exectime-stats]])
  (:use [slacker.interceptors.slowwatchdog :only [slow-watch-dog]]))

(defn -main [& args]
  (start-slacker-server [(the-ns 'redday.stats)
                         (the-ns 'redday.api)]
                        6565
                        :interceptors (interceptors [exectime-stats
                                                     (slow-watch-dog 1000)]))
  (println "server started on 6565."))

